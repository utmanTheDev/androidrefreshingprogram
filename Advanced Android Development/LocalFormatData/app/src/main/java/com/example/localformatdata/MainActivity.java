package com.example.localformatdata;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    private NumberFormat mNumberFormat = NumberFormat.getInstance();
    private static final String TAG = MainActivity.class.getSimpleName();
    private NumberFormat mCurrencyFormat = NumberFormat.getCurrencyInstance();

    // Fixed price in U.S. dollars and cents: ten cents.
    private double mPrice = 0.10;
    // Exchange rates for France (FR) and Israel (IW).
    private double mFrExchangeRate = 0.93; // 0.93 euros = $1.
    private double mIwExchangeRate = 3.61; // 3.61 new shekels = $1.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showHelp();
            }
        });

        // Get the current date.
        final Date myDate = new Date();
        // Add 5 days in milliseconds to create the expiration date.
        final long expirationDate = myDate.getTime() + TimeUnit.DAYS.toMillis(5);
        // Set the expiration date as the date to display.
        myDate.setTime(expirationDate);

        String myFormattedDate = DateFormat.getDateInstance().format(myDate);
        // Display the formatted date.
        TextView expirationDateView = findViewById(R.id.date);
        expirationDateView.setText(myFormattedDate);

        final TextView totalToPay = findViewById(R.id.total);

        EditText enteredQuantity = findViewById(R.id.quantity);
        enteredQuantity.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int action, KeyEvent keyEvent) {
                if (action == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager inputMethodManager = (InputMethodManager) textView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(textView.getWindowToken(), 0);

                    try {
                        int mInputQuantity = mNumberFormat.parse(textView.getText().toString()).intValue();
                        // Convert to string using locale's number format.
                        String myFormattedQuantity = mNumberFormat.format(mInputQuantity);
                        // Show the locale-formatted quantity.
                        textView.setText(myFormattedQuantity);
                        textView.setError(null);

                        String formattedTotalDivisa = mCurrencyFormat.format(mInputQuantity * mPrice);
                        totalToPay.setText(formattedTotalDivisa);

                    } catch (ParseException e) {
                        Log.e(TAG, Log.getStackTraceString(e));
                        textView.setError(getText(R.string.enter_number));
                        return true;
                    }
                }

                return false;
            }
        });

        String myFormattedPrice;
        String deviceLocale = Locale.getDefault().getCountry();
        // If country code is France or Israel, calculate price
        // with exchange rate and change to the country's currency format.
        if (deviceLocale.equals("ES") || deviceLocale.equals("IL")) {
            if (deviceLocale.equals("ES")) {
                // Calculate mPrice in euros.
                mPrice *= mFrExchangeRate;
            } else {
                // Calculate mPrice in new shekels.
                mPrice *= mIwExchangeRate;
            }
            // Use the user-chosen locale's currency format, which
            // is either France or Israel.
            myFormattedPrice = mCurrencyFormat.format(mPrice);
        } else {
            // mPrice is the same (based on U.S. dollar).
            // Use the currency format for the U.S.
            mCurrencyFormat = NumberFormat.getCurrencyInstance(Locale.US);
            myFormattedPrice = mCurrencyFormat.format(mPrice);
        }
        TextView localePrice = findViewById(R.id.price);
        localePrice.setText(myFormattedPrice);
    }


    /**
     * Shows the Help screen.
     */
    private void showHelp() {
        // Create the intent.
        Intent helpIntent = new Intent(this, HelpActivity.class);
        // Start the HelpActivity.
        startActivity(helpIntent);
    }

    public void goToScoreActivity(View view) {
        Intent helpIntent = new Intent(this, ScoreActivity.class);
        startActivity(helpIntent);
    }

    /**
     * Creates the options menu and returns true.
     *
     * @param menu Options menu
     * @return boolean   True after creating options menu.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Handles options menu item clicks.
     *
     * @param item Menu item
     * @return boolean  True if menu item is selected.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle options menu item clicks here.
        int id = item.getItemId();
        if (id == R.id.action_help) {
            Intent intent = new Intent(this, HelpActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_language) {
            Intent languageIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(languageIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}