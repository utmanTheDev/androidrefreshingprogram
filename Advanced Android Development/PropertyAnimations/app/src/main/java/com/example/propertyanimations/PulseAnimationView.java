package com.example.propertyanimations;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;

import androidx.dynamicanimation.animation.DynamicAnimation;
import androidx.dynamicanimation.animation.FlingAnimation;
import androidx.dynamicanimation.animation.SpringAnimation;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;

import static androidx.dynamicanimation.animation.SpringForce.STIFFNESS_LOW;

public class PulseAnimationView extends View {
    private AnimatorSet pulseAnimatorSet = new AnimatorSet();

    private static final int ANIMATION_DURATION = 4000;
    private static final long ANIMATION_DELAY = 1000;
    private static final int COLOR_ADJUSTER = 5;

    private final Paint paint = new Paint();
    private float px, py;
    private float radius;

    public void setRadius(float radius) {
        this.radius = radius;
        paint.setColor(Color.GREEN + (int) radius / COLOR_ADJUSTER);
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(px, py, radius, paint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            px = event.getX();
            py = event.getY();

            if (pulseAnimatorSet != null && pulseAnimatorSet.isRunning()) {
                pulseAnimatorSet.cancel();
            }
            pulseAnimatorSet.start();
        }

        return super.onTouchEvent(event);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        ObjectAnimator growAnimator = ObjectAnimator.ofFloat(this, "radius", 0, getWidth());
        growAnimator.setInterpolator(new LinearInterpolator());
        growAnimator.setDuration(ANIMATION_DURATION);

        ObjectAnimator shrinkAnimator = ObjectAnimator.ofFloat(this, "radius", getWidth(), 0);
        shrinkAnimator.setInterpolator(new LinearOutSlowInInterpolator());
        shrinkAnimator.setDuration(ANIMATION_DURATION);
        shrinkAnimator.setStartDelay(ANIMATION_DELAY);

        ObjectAnimator repeatAnimator = ObjectAnimator.ofFloat(this, "radius", 0, getWidth());
        repeatAnimator.setStartDelay(ANIMATION_DELAY);
        repeatAnimator.setDuration(ANIMATION_DURATION);

        repeatAnimator.setRepeatCount(1);
        repeatAnimator.setRepeatMode(ValueAnimator.REVERSE);

        pulseAnimatorSet.play(growAnimator).before(shrinkAnimator);
        pulseAnimatorSet.play(repeatAnimator).before(shrinkAnimator);

        final SpringAnimation anim = new SpringAnimation(this, DynamicAnimation.Y, 10).setStartVelocity(10000);
        anim.getSpring().setStiffness(STIFFNESS_LOW);
        anim.start();

        FlingAnimation fling = new FlingAnimation(this, DynamicAnimation.ROTATION_X);
        fling.setStartVelocity(150)
                .setMinValue(0)
                .setMaxValue(1000)
                .setFriction(0.1f)
                .start();
    }

    public PulseAnimationView(Context context) {
        super(context, null);
    }

    public PulseAnimationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PulseAnimationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
