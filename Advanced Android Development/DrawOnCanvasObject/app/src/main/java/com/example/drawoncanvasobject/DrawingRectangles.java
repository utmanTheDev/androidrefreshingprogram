package com.example.drawoncanvasobject;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.core.content.res.ResourcesCompat;

public class DrawingRectangles extends View {
    private static final float TOUCH_TOLERANCE = 20;
    private int backgroundColor;
    private int drawColor;
    private float X;
    private float Y;
    private Canvas extraCanvas;
    private Bitmap extraBitmap;
    private Paint paint;
    private Path path;

    public DrawingRectangles(Context context, AttributeSet attributeSet) {
        super(context);

        backgroundColor = ResourcesCompat.getColor(getResources(), R.color.backgroundColor, null);
        drawColor = ResourcesCompat.getColor(getResources(), R.color.rectableColor, null);

        path = new Path();
        paint = new Paint();
        paint.setColor(drawColor);
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(12);
    }

    private void touchStart(float x, float y) {
        path.moveTo(x, y);
        X = x;
        Y = y;
    }

    private void touchMove(float x, float y) {
        float dx = Math.abs(x - X);
        float dy = Math.abs(y - Y);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            int r = Math.round(x);
            int s = Math.round(y);
            int r1 = Math.round(dx);
            int s1 = Math.round(dy);
            Rect rect = new Rect(r, s, r1, s1);
            extraCanvas.drawRect(rect, paint);
            X = x;
            Y = y;
        }
    }

    private void touchUp() {
        path.reset();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchStart(x, y);
                break;
            case MotionEvent.ACTION_MOVE:
                touchMove(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touchUp();
                break;
            default:
                // Nothing to do here!
        }
        return true;
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);
        extraBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        extraCanvas = new Canvas(extraBitmap);
        extraCanvas.drawColor(backgroundColor);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(extraBitmap, 0, 0, null);
    }

    public DrawingRectangles(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
