package com.example.devicelocation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.List;


public class MainActivity extends AppCompatActivity implements FetchAddressTask.OnTaskCompleted {
    private static final int REQUEST_LOCATION_PERMISSION = 1;
    private static final String TRACKING_LOCATION_KEY = "tracking_location";

    private Button btnLocation;
    private TextView txtLocation;
    private ImageView imgAndroid;

    private boolean trackingLocation;
    private FusedLocationProviderClient locationProviderClient;
    private LocationCallback locationCallback;

    private AnimatorSet animatorSet;

    private float speed, meters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLocation = findViewById(R.id.button_location);
        txtLocation = findViewById(R.id.textview_location);
        imgAndroid = findViewById(R.id.imageview_android);

        locationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.rotate);
        animatorSet.setTarget(imgAndroid);

        if (savedInstanceState != null) {
            trackingLocation = savedInstanceState.getBoolean(TRACKING_LOCATION_KEY);
        }

        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!trackingLocation) {
                    startTrackingLocation();
                } else {
                    stopTrackingLocation();
                }
            }
        });

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                // If tracking is turned on, reverse geocode into an address
                if (trackingLocation) {
                    new FetchAddressTask(MainActivity.this, MainActivity.this).execute(locationResult.getLastLocation());
                    meters = locationResult.getLastLocation().getAccuracy();
                    speed = locationResult.getLastLocation().getSpeed();
                }
            }
        };
    }

    private void startTrackingLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSION);
        } else {
            trackingLocation = true;
            locationProviderClient.requestLocationUpdates(getLocationRequest(), locationCallback, null);

            txtLocation.setText(getString(R.string.address_text,
                    getString(R.string.loading),
                    System.currentTimeMillis(),
                    getString(R.string.loading),
                    getString(R.string.loading)
                    )
            );
            btnLocation.setText(getString(R.string.stop_tracking_location));
            animatorSet.start();
        }
    }

    private void stopTrackingLocation() {
        if (trackingLocation) {
            trackingLocation = false;
            btnLocation.setText(R.string.start_tracking_location);
            txtLocation.setText(R.string.textview_hint);
            animatorSet.end();
        }
    }

    private LocationRequest getLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(TRACKING_LOCATION_KEY, trackingLocation);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startTrackingLocation();
                } else {
                    Toast.makeText(this, R.string.location_permission_denied, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onTaskCompleted(String result) {
        if (trackingLocation) {
            txtLocation.setText(getString(R.string.address_text, result, System.currentTimeMillis(), meters, speed));
        }
    }

    @Override
    protected void onPause() {
        if (trackingLocation) {
            stopTrackingLocation();
            trackingLocation = true;
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (trackingLocation) {
            startTrackingLocation();
        }
        super.onResume();
    }

}
