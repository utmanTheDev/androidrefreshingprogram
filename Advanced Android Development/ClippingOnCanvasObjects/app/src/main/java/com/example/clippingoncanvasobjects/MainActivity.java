package com.example.clippingoncanvasobjects;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Build;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new ClippedView(this));
    }

    private static class ClippedView extends View {
        private int mSmallRectOffset = (int) getResources().getDimension(R.dimen.smallRectOffset);
        private int mClipRectBottom = (int) getResources().getDimension(R.dimen.clipRectBottom);
        private int mClipRectRight = (int) getResources().getDimension(R.dimen.clipRectRight);
        private int mClipRectLeft = (int) getResources().getDimension(R.dimen.clipRectLeft);
        private int mClipRectTop = (int) getResources().getDimension(R.dimen.clipRectTop);
        private int mRectInset = (int) getResources().getDimension(R.dimen.rectInset);

        private int mCircleRadius = (int) getResources().getDimension(R.dimen.circleRadius);
        private int mTextOffset = (int) getResources().getDimension(R.dimen.textOffset);
        private int mTextSize = (int) getResources().getDimension(R.dimen.textSize);

        private int mColumnOne = mRectInset;
        private int mColumnnTwo = mColumnOne + mRectInset + mClipRectRight;

        private int mRowOne = mRectInset;
        private int mRowTwo = mRowOne + mRectInset + mClipRectBottom;
        private int mRowThree = mRowTwo + mRectInset + mClipRectBottom;
        private int mRowFour = mRowThree + mRectInset + mClipRectBottom;
        private int mTextRow = mRowFour + (int) (1.5 * mClipRectBottom);

        private RectF rectF;
        private Paint paint;
        private Path path;

        private void drawClippedRectangle(Canvas canvas) {
            canvas.clipRect(mClipRectLeft, mClipRectTop, mClipRectRight, mClipRectBottom);
            canvas.drawColor(Color.WHITE);

            // Change the color to red and
            // draw a line inside the clipping rectangle.
            paint.setColor(Color.RED);
            canvas.drawLine(mClipRectLeft, mClipRectTop, mClipRectRight, mClipRectBottom, paint);

            // Set the color to green and
            // draw a circle inside the clipping rectangle.
            paint.setColor(Color.GREEN);
            canvas.drawCircle(mCircleRadius, mClipRectBottom - mCircleRadius, mCircleRadius, paint);

            // Set the color to blue and draw text aligned with the right edge
            // of the clipping rectangle.
            paint.setColor(Color.BLUE);
            // Align the RIGHT side of the text with the origin.
            paint.setTextAlign(Paint.Align.RIGHT);
            canvas.drawText(getContext().getString(R.string.clipping), mClipRectRight, mTextOffset, paint);
        }


        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawColor(Color.GRAY);
            /**VERSIO 1 **/
            canvas.save();
            canvas.translate(mColumnOne, mRowOne);
            canvas.clipRect(2 * mRectInset, 2 * mRectInset, mClipRectRight - 2 * mRectInset, mClipRectBottom - 2 * mRectInset);
            drawClippedRectangle(canvas);
            canvas.restore();

            /**VERSIO 2 **/
            canvas.save();
            // Move the origin to the right for the next rectangle.
            canvas.translate(mColumnnTwo, mRowOne);
            // Use the subtraction of two clipping rectangles to create a frame.
            canvas.clipRect(2 * mRectInset, 2 * mRectInset,
                    mClipRectRight - 2 * mRectInset, mClipRectBottom - 2 * mRectInset);
            // The method clipRect(float, float, float, float, Region.Op
            // .DIFFERENCE) was deprecated in API level 26. The recommended
            // alternative method is clipOutRect(float, float, float, float),
            // which is currently available in API level 26 and higher.
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
                canvas.clipRect(4 * mRectInset, 4 * mRectInset, mClipRectRight - 4 * mRectInset, mClipRectBottom - 4 * mRectInset, Region.Op.DIFFERENCE);
            else {
                canvas.clipOutRect(4 * mRectInset, 4 * mRectInset, mClipRectRight - 4 * mRectInset, mClipRectBottom - 4 * mRectInset);
            }
            drawClippedRectangle(canvas);
            canvas.restore();

            /**VERSIO 3 **/
            canvas.save();
            canvas.translate(mColumnOne, mRowTwo);
            // Clears any lines and curves from the path but unlike reset(),
            // keeps the internal data structure for faster reuse.
            path.rewind();
            path.addCircle(mCircleRadius, mClipRectBottom - mCircleRadius, mCircleRadius, Path.Direction.CCW);
            // The method clipPath(path, Region.Op.DIFFERENCE) was deprecated in
            // API level 26. The recommended alternative method is
            // clipOutPath(Path), which is currently available in
            // API level 26 and higher.
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                canvas.clipPath(path, Region.Op.DIFFERENCE);
            } else {
                canvas.clipOutPath(path);
            }
            drawClippedRectangle(canvas);
            canvas.restore();


            /**VERSIO 4 **/
            canvas.save();
            canvas.translate(mColumnnTwo, mRowTwo);
            canvas.clipRect(mClipRectLeft, mClipRectTop,
                    mClipRectRight - mSmallRectOffset,
                    mClipRectBottom - mSmallRectOffset);
            // The method clipRect(float, float, float, float, Region.Op
            // .INTERSECT) was deprecated in API level 26. The recommended
            // alternative method is clipRect(float, float, float, float), which
            // is currently available in API level 26 and higher.
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                canvas.clipRect(mClipRectLeft + mSmallRectOffset,
                        mClipRectTop + mSmallRectOffset, mClipRectRight,
                        mClipRectBottom, Region.Op.INTERSECT);
            } else {
                canvas.clipRect(mClipRectLeft + mSmallRectOffset,
                        mClipRectTop + mSmallRectOffset, mClipRectRight,
                        mClipRectBottom);
            }

            drawClippedRectangle(canvas);
            canvas.restore();

        }

        public ClippedView(Context context) {
            this(context, null);
        }

        public ClippedView(Context context, AttributeSet attrs) {
            super(context, attrs);
            setFocusable(true);
            paint = new Paint();
            path = new Path();

            paint.setAntiAlias(true);
            paint.setStrokeWidth((int) getResources().getDimension(R.dimen.strokeWidth));
            paint.setTextSize((int) getResources().getDimension(R.dimen.textSize));

            rectF = new RectF(new Rect(mRectInset, mRectInset, mClipRectRight - mRectInset, mClipRectBottom - mRectInset));
        }

        public ClippedView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }
    }
}
