package com.example.getsensordata;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity implements SensorEventListener {
    private TextView txtSensorLight, txtSensorProximity, txtSensorRelativeHumidity;
    private Sensor sensorProximity, sensorLight, sensorRelativeHumidity;
    private SensorManager sensorManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

        txtSensorRelativeHumidity = findViewById(R.id.label_relative_humidity);
        txtSensorProximity = findViewById(R.id.label_proximity);
        txtSensorLight = findViewById(R.id.label_light);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorRelativeHumidity = sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        sensorProximity = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        sensorLight = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        String sensorError = getResources().getString(R.string.error_no_sensor);

        if (sensorRelativeHumidity == null) txtSensorRelativeHumidity.setText(sensorError);
        if (sensorProximity == null) txtSensorProximity.setText(sensorError);
        if (sensorLight == null) txtSensorLight.setText(sensorError);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        int sensorType = sensorEvent.sensor.getType();
        float currentValue = sensorEvent.values[0];

        switch (sensorType) {
            case Sensor.TYPE_LIGHT:
                txtSensorLight.setText(getResources().getString(R.string.label_light, currentValue));

                int currentValueInt = Math.round(currentValue);
                if (isBetween(currentValueInt, 0, 200))
                    getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.colorLevel_1));
                else if (isBetween(currentValueInt, 201, 400))
                    getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.colorLevel_2));
                else if (isBetween(currentValueInt, 401, 600))
                    getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.colorLevel_3));
                else if (isBetween(currentValueInt, 601, 800))
                    getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.colorLevel_4));
                else if (isBetween(currentValueInt, 801, 1100))
                    getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.colorLevel_5));

                break;
            case Sensor.TYPE_PROXIMITY:
                txtSensorProximity.setText(getResources().getString(R.string.label_proximity, currentValue));
                break;
            case Sensor.TYPE_RELATIVE_HUMIDITY:
                txtSensorRelativeHumidity.setText(getResources().getString(R.string.label_relative_humidity, currentValue));
            default:
                // do nothing
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (sensorProximity != null) {
            sensorManager.registerListener(this, sensorProximity, SensorManager.SENSOR_DELAY_NORMAL);
        }

        if (sensorLight != null) {
            sensorManager.registerListener(this, sensorLight, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        sensorManager.unregisterListener(this);
    }

    public static boolean isBetween(int x, int lower, int upper) {
        return lower <= x && x <= upper;
    }
}
