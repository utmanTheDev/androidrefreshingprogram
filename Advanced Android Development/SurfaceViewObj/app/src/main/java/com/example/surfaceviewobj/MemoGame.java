package com.example.surfaceviewobj;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Collections;

public class MemoGame extends SurfaceView implements Runnable {
    private ArrayList<Integer> valorCartes = new ArrayList<>();
    private ArrayList<Carta> cartesTablero = new ArrayList<>();
    private Thread gameThread;
    private Context context;

    private SurfaceHolder mSurfaceHolder;

    private boolean mRunning;

    private int posCarta1, posCarta2;
    private int numSelect = 0;
    private int numCarta = 0;
    private int numCols = 4;
    private int padding = 5;
    private int cardSize;

    private Paint dpaint;
    private Paint fpaint;
    private Paint tpaint;
    private Paint ppaint;

    public MemoGame(Context context) {
        super(context);
        init(context);
    }

    public MemoGame(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MemoGame(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @Override
    public void run() {
        Canvas canvas;
        while (mRunning) {
            if (mSurfaceHolder.getSurface().isValid()) {
                canvas = mSurfaceHolder.lockCanvas();
                canvas.drawColor(Color.WHITE);
                for (Carta carta : cartesTablero) {
                    switch (carta.status) {
                        case CARA:
                            canvas.drawRect(carta.rect, fpaint);
                            canvas.drawText(String.valueOf(carta.numCarta), carta.rect.left + (cardSize / 2), carta.rect.top + (cardSize / 2), tpaint);
                            if (numSelect == 2) {
                                Carta carta1 = cartesTablero.get(posCarta1);
                                Carta carta2 = cartesTablero.get(posCarta2);

                                if (carta1.numCarta == carta2.numCarta) {
                                    cartesTablero.get(posCarta1).status = Status.PAREJA;
                                    cartesTablero.get(posCarta2).status = Status.PAREJA;
                                } else {
                                    cartesTablero.get(posCarta1).status = Status.DORSO;
                                    cartesTablero.get(posCarta2).status = Status.DORSO;
                                }
                                numSelect = 0;
                                invalidate();
                            }
                            break;
                        case DORSO:
                            canvas.drawRect(carta.rect.left, carta.rect.top, carta.rect.right, carta.rect.bottom, dpaint);
                            break;
                        case PAREJA:
                            canvas.drawRect(carta.rect, ppaint);
                            canvas.drawText(String.valueOf(carta.numCarta), carta.rect.left + (cardSize / 2), carta.rect.top + (cardSize / 2), tpaint);
                            break;
                    }
                }
                mSurfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

    public void pause() {
        mRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
        }
    }

    public void resume() {
        mRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    private void handleCardClick(float x, float y) {
        for (int i = 0; i < cartesTablero.size(); i++) {
            if (cartesTablero.get(i).rect.contains(Math.round(x), Math.round(y))) {
                if (cartesTablero.get(i).status == Status.DORSO) {
                    cartesTablero.get(i).status = Status.CARA;
                    numSelect++;
                    if (numSelect == 1) posCarta1 = i;
                    if (numSelect == 2) posCarta2 = i;
                    invalidate();
                }
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                handleCardClick(event.getX(), event.getY());
                break;
            default:
                //Nothing to do here
        }
        return true;
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldw, int oldh) {
        super.onSizeChanged(width, height, oldw, oldh);
        if (width < height)
            cardSize = (getWidth() / numCols) - (padding / numCols);
        else
            cardSize = (getHeight() / numCols) - (padding / numCols);
        crearTablero();
    }

    private void crearTablero() {
        for (int i = 0; i < numCols; i++) {
            cartesTablero.add(new Carta(valorCartes.get(numCarta), Status.DORSO, new Rect(padding, padding + cardSize * i, cardSize, cardSize * (i + 1)), numCarta));
            numCarta++;
            for (int j = 1; j < numCols; j++) {
                cartesTablero.add(new Carta(valorCartes.get(numCarta), Status.DORSO, new Rect(padding + (cardSize * j), padding + cardSize * i, cardSize * (j + 1), cardSize * (i + 1)), numCarta));
                numCarta++;
            }
        }
    }

    public void init(Context context) {
        this.context = context;
        mSurfaceHolder = getHolder();
        dpaint = new Paint();
        dpaint.setColor(Color.LTGRAY);

        fpaint = new Paint();
        fpaint.setColor(Color.CYAN);

        ppaint = new Paint();
        ppaint.setColor(Color.GREEN);

        tpaint = new Paint();
        tpaint.setColor(Color.BLACK);
        tpaint.setTextAlign(Paint.Align.RIGHT);
        tpaint.setAntiAlias(true);
        tpaint.setTextSize((int) getResources().getDimension(R.dimen.textSize));

        for (int i = 0; i < (numCols * 2); i++) valorCartes.add(i);
        valorCartes.addAll(valorCartes);
        Collections.shuffle(valorCartes);
    }

    class Carta {
        int numCarta;
        Status status;
        Rect rect;
        int position;

        Carta(int numCarta, Status status, Rect rect, int position) {
            this.numCarta = numCarta;
            this.status = status;
            this.rect = rect;
            this.position = position;
        }

        @Override
        public String toString() {
            return "Carta{" +
                    "numCarta=" + numCarta +
                    ", status=" + status +
                    ", rect=" + rect +
                    ", position=" + position +
                    '}';
        }
    }

    enum Status {DORSO, CARA, PAREJA}
}

