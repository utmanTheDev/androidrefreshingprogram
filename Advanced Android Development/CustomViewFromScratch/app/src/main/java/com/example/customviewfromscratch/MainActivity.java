package com.example.customviewfromscratch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {
    DialView dialView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dialView = findViewById(R.id.dialView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.selection_3:
                dialView.setSelectionCount(3);
                return true;
            case R.id.selection_4:
                dialView.setSelectionCount(4);
                return true;
            case R.id.selection_5:
                dialView.setSelectionCount(5);
                return true;
            case R.id.selection_6:
                dialView.setSelectionCount(6);
                return true;
            case R.id.selection_7:
                dialView.setSelectionCount(7);
                return true;
            default:
                //Nothing to do
        }
        return super.onOptionsItemSelected(item);
    }
}
