package com.example.fragmentscommunication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SimpleFragment.OnFragmentInteractionListener {
    static final String STATE_FRAGMENT = "state_of_fragment";
    private boolean isFragmentDisplayed = false;
    private int mRadioButtonChoice = 2; // The default (no choice).
    private Button openButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            isFragmentDisplayed = savedInstanceState.getBoolean(STATE_FRAGMENT);
            if (isFragmentDisplayed) {
                openButton.setText(R.string.close);
            }
        }

        openButton = findViewById(R.id.open_button);
        openButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isFragmentDisplayed) {
                    displaySimpleFragment();
                } else {
                    closeSimpleFragment();
                }
            }
        });
    }

    public void displaySimpleFragment() {
        SimpleFragment fragment = SimpleFragment.newInstance(mRadioButtonChoice);

        //SimpleFragment simpleFragment = SimpleFragment.newInstance();
        SimpleFragment simpleFragment = SimpleFragment.newInstance(mRadioButtonChoice);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.add(R.id.fragment_container, simpleFragment).addToBackStack(null).commit();
        openButton.setText(R.string.close);
        isFragmentDisplayed = true;
    }

    public void closeSimpleFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        SimpleFragment simpleFragment = (SimpleFragment) fragmentManager.findFragmentById(R.id.fragment_container);

        if (simpleFragment != null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(simpleFragment).commit();
        }
        openButton.setText(R.string.open);
        isFragmentDisplayed = false;
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the state of the fragment (true=open, false=closed).
        savedInstanceState.putBoolean(STATE_FRAGMENT, isFragmentDisplayed);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRadioButtonChoice(int choice) {
        String msg = "None";
        mRadioButtonChoice = choice;
        if (choice == 0) msg = "Yes";
        else if (choice == 1) msg = "No";

        Toast.makeText(this, "Choice is " + msg, Toast.LENGTH_SHORT).show();
    }
}
