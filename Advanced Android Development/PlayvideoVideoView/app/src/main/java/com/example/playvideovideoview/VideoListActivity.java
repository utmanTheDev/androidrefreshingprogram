package com.example.playvideovideoview;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class VideoListActivity extends AppCompatActivity {
    private static final ArrayList<String> listOfVideos = new ArrayList<>();
    private VideoListAdapter videoListAdapter;
    private RecyclerView recyclerViewVideos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_list);

        listOfVideos.add("pexels_videos_1429889");
        listOfVideos.add("pexels_videos_1572432");
        listOfVideos.add("pexels_videos_1739011");
        listOfVideos.add("pexels_videos_8729889");

        videoListAdapter = new VideoListAdapter(this, listOfVideos);
        recyclerViewVideos = findViewById(R.id.rv_videos);
        recyclerViewVideos.setAdapter(videoListAdapter);
        recyclerViewVideos.setLayoutManager(new LinearLayoutManager(this));
    }
}
