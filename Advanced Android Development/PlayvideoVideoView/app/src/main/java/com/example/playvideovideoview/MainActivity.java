package com.example.playvideovideoview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {
    //private static final String VIDEO_SAMPLE = "https://developers.google.com/training/images/tacoma_narrows.mp4";
    private static final String PLAYPAUSE_STATUS = "play_pause_status";
    private static final String VIDEO_SAMPLE = "pexels_videos_8729889";
    private static final String PLAYBACK_TIME = "play_time";
    private boolean btnPlayPauseStatus = true;
    private int currentPosition = 0;

    private Button btnPlayPause, btnNext10Secs, btnPrev10Secs;
    private MediaController mediaController;
    private TextView bufferingTextView;
    private VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            currentPosition = savedInstanceState.getInt(PLAYBACK_TIME);
            btnPlayPauseStatus = savedInstanceState.getBoolean(PLAYPAUSE_STATUS);
        }

        bufferingTextView = findViewById(R.id.buffering_textview);
        btnPlayPause = findViewById(R.id.btnPlayPause);
        btnNext10Secs = findViewById(R.id.next10Secs);
        btnPrev10Secs = findViewById(R.id.prev10Secs);
        videoView = findViewById(R.id.videoview);

        mediaController = new MediaController(this);
        mediaController.setMediaPlayer(videoView);
        videoView.setMediaController(mediaController);

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Toast.makeText(MainActivity.this, "Playback completed", Toast.LENGTH_SHORT).show();
                btnPrev10Secs.setEnabled(false);
                btnNext10Secs.setEnabled(false);
                btnPlayPause.setText("Play");
                videoView.seekTo(1);
            }
        });

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                bufferingTextView.setVisibility(VideoView.INVISIBLE);
                btnPrev10Secs.setEnabled(true);
                btnNext10Secs.setEnabled(true);
                btnPlayPause.setEnabled(true);
                if (currentPosition > 0) {
                    videoView.seekTo(currentPosition);
                } else {
                    videoView.seekTo(1);
                }
            }
        });
    }

    private Uri getMedia(String mediaName) {
        if (URLUtil.isValidUrl(mediaName)) {
            return Uri.parse(mediaName);
        } else {
            return Uri.parse("android.resource://" + getPackageName() + "/raw/" + mediaName);
        }
    }

    private void initializePlayer() {
        bufferingTextView.setVisibility(VideoView.VISIBLE);
        Uri videoUri = getMedia(VIDEO_SAMPLE);
        videoView.setVideoURI(videoUri);

        if (currentPosition > 0) {
            videoView.seekTo(currentPosition);
        } else {
            videoView.seekTo(1);            // Skipping to 1 shows the first frame of the video.
        }
    }

    private void releasePlayer() {
        videoView.stopPlayback();
    }

    public void playPauseVideo(View view) {
        if (!btnPlayPauseStatus) {
            btnPrev10Secs.setEnabled(false);
            btnNext10Secs.setEnabled(false);
            btnPlayPause.setText("Play");
            videoView.pause();
        } else {
            btnPrev10Secs.setEnabled(true);
            btnNext10Secs.setEnabled(true);
            btnPlayPause.setText("Pause");
            videoView.start();
        }
        btnPlayPauseStatus = !btnPlayPauseStatus;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PLAYBACK_TIME, videoView.getCurrentPosition());
        outState.putBoolean(PLAYPAUSE_STATUS, btnPlayPauseStatus);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializePlayer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        releasePlayer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            videoView.pause();
        }
    }

    public void nextPrev10Secs(View view) {
        int currentPosition = videoView.getCurrentPosition();
        if (view.getId() == R.id.next10Secs) {
            videoView.seekTo(currentPosition + 10000);
        } else {
            if (currentPosition > 11)
                videoView.seekTo(currentPosition - 10000);
            else
                videoView.seekTo(0);
        }
    }

    public void goToListActivity(View view) {
        Intent intent = new Intent(this, VideoListActivity.class);
        startActivity(intent);
    }
}
