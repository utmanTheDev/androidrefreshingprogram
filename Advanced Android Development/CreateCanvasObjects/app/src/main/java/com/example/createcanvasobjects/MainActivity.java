package com.example.createcanvasobjects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Paint paintText = new Paint(Paint.UNDERLINE_TEXT_FLAG);
    private Paint paint = new Paint();
    private Bitmap bitmap;
    private Canvas canvas;

    private Rect bounds = new Rect();
    private Rect rect = new Rect();

    private ImageView imageView;

    private static final int OFFSET = 120;
    private int offset = OFFSET;

    private static final int MULTIPLIER = 100;

    private int colorBackground;
    private int colorRectangle;
    private int colorAccent;
    private int timesClick;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        colorBackground = ResourcesCompat.getColor(getResources(), R.color.colorBackground, null);
        colorRectangle = ResourcesCompat.getColor(getResources(), R.color.colorRectangle, null);
        colorAccent = ResourcesCompat.getColor(getResources(), R.color.colorAccent, null);

        paint.setColor(colorBackground);
        paintText.setColor(ResourcesCompat.getColor(getResources(), R.color.colorPrimaryDark, null));
        paintText.setTextSize(70);

        imageView = findViewById(R.id.my_imageview);
    }

    public void drawSomething(View view) {
        int vWidth = view.getWidth();
        int vHeight = view.getHeight();
        int halfWidth = vWidth / 2;
        int halfHeight = vHeight / 2;
        if (offset == OFFSET) {
            initializeCanvas(vWidth, vHeight);
        } else {
            if (offset < halfWidth && offset < halfHeight) {
                paint.setColor(colorRectangle - MULTIPLIER * offset);
                rect.set(offset, offset, vWidth - offset, vHeight - offset);
                canvas.drawRect(rect, paint);
                offset += OFFSET;
            } else {
                String text;
                timesClick += 1;
                if (timesClick == 2) {
                    Toast.makeText(this, "Click again to clear canvas", Toast.LENGTH_SHORT).show();
                } else if (timesClick > 2) {
                    initializeCanvas(vWidth, vHeight);
                    offset = OFFSET;
                    timesClick = 0;
                } else {
                    text = getString(R.string.done);
                    paint.setColor(colorAccent);
                    canvas.drawCircle(halfWidth, halfHeight, halfWidth / 3, paint);
                    paintText.getTextBounds(text, 0, text.length(), bounds);
                    int x = halfWidth - bounds.centerX();
                    int y = halfHeight - bounds.centerY();
                    canvas.drawText(text, x, y, paintText);
                }
            }
        }
        view.invalidate();
    }

    private void initializeCanvas(int vWidth, int vHeight) {
        bitmap = Bitmap.createBitmap(vWidth, vHeight, Bitmap.Config.ARGB_8888);
        imageView.setImageBitmap(bitmap);
        canvas = new Canvas(bitmap);
        canvas.drawColor(colorBackground);
        canvas.drawText(getString(R.string.keep_tapping), 100, 100, paintText);
        offset += OFFSET;
    }
}
