package com.example.sensorbasedorientation;

public class DeviceInfo {
    float azimuth;
    float pitch;
    float roll;

    DeviceInfo(float azimuth, float pitch, float roll) {
        this.azimuth = azimuth;
        this.pitch = pitch;
        this.roll = roll;
    }
}