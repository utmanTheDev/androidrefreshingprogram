import java.util.*

fun main(args: Array<String>) {
    //dayOfWeek()
    getFortuneCookie()
    //val rollDice = { Random().nextInt(12) + 1 }
    //val rollDice = { sides: Int ->
    //    Random().nextInt(sides) + 1
    //}

}

fun dayOfWeek() {
    println("What day is it today ?")
    val day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)

    when (day) {
        1 -> println("Sunday")
        2 -> println("Monday")
        3 -> println("Tuesday")
        4 -> println("Wednesday")
        5 -> println("Thursday")
        6 -> println("Friday")
        7 -> println("Saturday")
        else -> println("Error")
    }
}

fun greetings(hour: Int) {
    if (hour < 12)
        println("Good morning, Kotlin")
    else
        println("Good night, Kotlin")
}

fun getFortuneCookie(): String {
    val fortunes = listOf(
        "You will have a great day!",
        "Things will go well for you today.",
        "Be humble and all will turn out well.",
        "Take it easy and enjoy life!"
    )
    val birthday = readLine()?.toIntOrNull() ?: 1
    return fortunes[birthday.rem(fortunes.size)]
}

fun canAddFish(tankSize: Int, currentFish: List<Int>, fishSize: Int = 2, hasDecoration: Boolean = true): Boolean {
    return (tankSize * if (hasDecoration) 0.8 else 1.0) >= (currentFish.sum() + fishSize)
}

fun whatShouldIDoToday(mood: String, weather: String = "sunny", temperature: Int = 24) = when (true) {
    mood == "happy" && weather == "sunny" -> println("Go for a walk")
    mood == "sad" && weather == "rainy" && temperature == 0 -> "stay in bed"
    temperature > 35 -> "go swimming"
    else -> println("Stay home and read")
}

fun practiceTimeCurries() {
    val spices = listOf("curry", "pepper", "cayenne", "ginger", "red curry", "green curry", "red pepper")
    var curries = spices.filter { it.contains("curry") }.sortedBy { it.length }
    var ce = spices.filter { it.startsWith("c") && it.endsWith("e") }
    var tres = spices.take(3).filter { it.startsWith('c') }
}




