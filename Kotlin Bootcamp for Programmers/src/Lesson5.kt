import kotlin.random.Random

const val MAX_BORROW = 5

class Book2(var title: String, var author: String, var year: Int, var pages: Int) {
    companion object {
        val BASE_URL = "http://www.turtlecare.net/"
    }

    fun getTitleAndAuthor(): Pair<String, String> {
        return (title to author)
    }

    fun getTitleAuthorAndYear(): Triple<String, String, Int> {
        return Triple(title, author, year)
    }

    fun canBorrow(hasBooks: Int): Boolean {
        return (hasBooks < MAX_BORROW)
    }

    fun printUrl() {
        println("$BASE_URL$title.html")
    }

    fun weight(): Double = pages * 1.5
}

fun Book2.tornPages(torn: Int) = if (pages >= torn) pages -= torn else pages = 0

class Puppy() {
    fun playWithBook(book: Book2) {
        book.tornPages(Random.nextInt(12))
    }
}

open class BaseBuildingMaterial {
    open var numberNeeded = 1
}

class Wood : BaseBuildingMaterial() {
    override var numberNeeded: Int = 4
}

class Brick : BaseBuildingMaterial() {
    override var numberNeeded: Int = 8
}

val filter = { println("Filtered") }

class Building<out T : BaseBuildingMaterial>(val buildingMaterial: T) {
    val baseMaterialNeeded = 100
    val actualMaterialsNeeded = buildingMaterial.numberNeeded * baseMaterialNeeded

    fun build() {
        println(" $actualMaterialsNeeded ${buildingMaterial::class.simpleName} required")
    }
}

fun <T : BaseBuildingMaterial> isSmallBuilding(building: Building<T>) {
    if (building.actualMaterialsNeeded < 500) println("Small building")
    else println("large building")
}

//973 23 94 72

fun main(args: Array<String>) {
    var book = Book2("El hobbit", "J.R. Tolkien", 1982, 325)
    var info = book.getTitleAuthorAndYear()
    println("Here is your book ${info.first} written by ${info.second} in ${info.third} ")

    // PRACTICE
    val allBooks = setOf("Libro num1", "Libro num2", "Libro num3", "Libro num4")
    val library = mapOf("Shakespeare" to allBooks)
    println(library.any { it.value.contains("Hamlet") })

    val moreBooks = mutableMapOf<String, String>("Libro num5" to "Libro num6")
    moreBooks.getOrPut("Jungle Book") { "Kipling" }
    moreBooks.getOrPut("Hamlet") { "Shakespeare" }
    println(moreBooks)

    Building(Wood()).build()
    isSmallBuilding(Building(Brick()))

}

