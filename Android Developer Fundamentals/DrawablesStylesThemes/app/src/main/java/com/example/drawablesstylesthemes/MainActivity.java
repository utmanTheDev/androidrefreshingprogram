package com.example.drawablesstylesthemes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    static final String STATE_SCORE_1 = "Team 1 Score";
    static final String STATE_SCORE_2 = "Team 2 Score";
    private TextView txtScore1, txtScore2;
    private ImageView imgBatery;
    private int score1, score2, bateryLvl = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtScore1 = findViewById(R.id.score_1);
        txtScore2 = findViewById(R.id.score_2);
        imgBatery = findViewById(R.id.img_batery);

        if (savedInstanceState != null) {
            score1 = savedInstanceState.getInt(STATE_SCORE_1);
            score2 = savedInstanceState.getInt(STATE_SCORE_2);
            txtScore1.setText(String.valueOf(score1));
            txtScore2.setText(String.valueOf(score2));
        }
    }

    public void updateBateryLevel(View view) {
        int viewID = view.getId();
        switch (viewID) {
            case R.id.addBateryLevel:
                if (bateryLvl < 6)
                    imgBatery.setImageLevel(bateryLvl++);
                else
                    imgBatery.setImageLevel(6);
                break;
            case R.id.delBateryLevel:
                if (bateryLvl > 1)
                    imgBatery.setImageLevel(bateryLvl--);
                else
                    imgBatery.setImageLevel(0);
                break;
        }
    }

    public void decreaseScore(View view) {
        int viewID = view.getId();
        switch (viewID) {
            case R.id.decreaseTeam1:
                txtScore1.setText(String.valueOf(score1--));
                break;
            case R.id.decreaseTeam2:
                txtScore2.setText(String.valueOf(score2--));
        }
    }

    public void increaseScore(View view) {
        int viewID = view.getId();
        switch (viewID) {
            case R.id.increaseTeam1:
                txtScore1.setText(String.valueOf(score1++));
                break;
            case R.id.increaseTeam2:
                txtScore2.setText(String.valueOf(score2++));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        int nightMode = AppCompatDelegate.getDefaultNightMode();
        if (nightMode == AppCompatDelegate.MODE_NIGHT_YES) {
            menu.findItem(R.id.night_mode).setTitle(R.string.day_mode);
        } else {
            menu.findItem(R.id.night_mode).setTitle(R.string.night_mode);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.night_mode) {
            int nightMode = AppCompatDelegate.getDefaultNightMode();
            if (nightMode == AppCompatDelegate.MODE_NIGHT_YES) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
            recreate();
        }
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_SCORE_1, score1);
        outState.putInt(STATE_SCORE_2, score2);
        super.onSaveInstanceState(outState);
    }
}
