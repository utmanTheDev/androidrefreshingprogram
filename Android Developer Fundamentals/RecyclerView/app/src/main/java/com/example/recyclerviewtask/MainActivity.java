package com.example.recyclerviewtask;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {
    private LinkedList<String> wordList = new LinkedList<>();
    private WordListAdapter wordListAdapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        populateWordList();

        recyclerView = findViewById(R.id.recyclerview);
        wordListAdapter = new WordListAdapter(this, wordList);
        recyclerView.setAdapter(wordListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void addItemToList(View view) {
        int wordListSize = wordList.size();
        wordList.addLast("+ Word item" + wordListSize);
        recyclerView.getAdapter().notifyItemInserted(wordListSize);
        recyclerView.smoothScrollToPosition(wordListSize);
    }

    public void resetListItems(View view) {
        for (int i = wordList.size()-1; i >= 20; i--) {
            wordList.remove(i);
            recyclerView.getAdapter().notifyItemRemoved(i);
        }
    }

    private void populateWordList() {
        for (int i = 0; i < 20; i++) {
            wordList.addLast("Word item " + i);
        }
    }

}
