package com.example.clickableimages;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.clickableimages.extra.MESSAGE";
    private String mOrderMassage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void toast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void handleImageClick(View view) {
        switch (view.getId()) {
            case R.id.img_donut:
                toast(getString(R.string.donut_order_message));
                mOrderMassage = getString(R.string.donut_order_message);
                break;
            case R.id.img_icecream:
                toast(getString(R.string.ice_cream_order_message));
                mOrderMassage = getString(R.string.ice_cream_order_message);
                break;
            case R.id.img_froyo:
                toast(getString(R.string.froyo_order_message));
                mOrderMassage = getString(R.string.froyo_order_message);
                break;
            default:
                Log.e("MainActivity", "handleImageClick error con el id");
                break;
        }
    }

    public void goToOrderActivity(View view) {
        Intent intent = new Intent(this, OrderActivity.class);
        intent.putExtra(EXTRA_MESSAGE, mOrderMassage);
        startActivity(intent);
    }
}
