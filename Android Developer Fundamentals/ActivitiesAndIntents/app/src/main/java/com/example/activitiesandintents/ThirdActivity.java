package com.example.activitiesandintents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class ThirdActivity extends AppCompatActivity {
    private static final String TAG = ThirdActivity.class.getSimpleName();
    private TextView txtPassenger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        txtPassenger = (TextView) findViewById(R.id.text_passenger);

        int opcion = getIntent().getIntExtra(MainActivity.EXTRA_OPTION, 0);
        // Ejemplo simple para recibir info
        switch (opcion) {
            case 1:
                txtPassenger.setText("Pasajero numero 1");
                break;
            case 2:
                txtPassenger.setText("Pasajero numero 2");
                break;
            case 3:
                txtPassenger.setText("Pasajero numero 3");
                break;
            default:
                Toast.makeText(this, "Error de info", Toast.LENGTH_LONG).show();
                break;

        }
    }
}
