package com.example.android.android_me.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.android.android_me.R;
import com.example.android.android_me.data.AndroidImageAssets;

public class AndroidMeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_me);

        if (savedInstanceState == null) {
            BodyPartFragment bodyPartFragment = new BodyPartFragment();
            BodyPartFragment headPartFragment = new BodyPartFragment();
            BodyPartFragment legsPartFragment = new BodyPartFragment();

            int headIndex = getIntent().getIntExtra("headIndex", 0);
            int bodyIndex = getIntent().getIntExtra("bodyIndex", 0);
            int legsIndex = getIntent().getIntExtra("legsIndex", 0);

            headPartFragment.setImageIds(AndroidImageAssets.getHeads());
            headPartFragment.setListIndex(headIndex);

            bodyPartFragment.setImageIds(AndroidImageAssets.getBodies());
            bodyPartFragment.setListIndex(bodyIndex);

            legsPartFragment.setImageIds(AndroidImageAssets.getLegs());
            legsPartFragment.setListIndex(legsIndex);

            FragmentManager fragmentManager = getSupportFragmentManager();

            fragmentManager.beginTransaction().add(R.id.head_container, headPartFragment).commit();
            fragmentManager.beginTransaction().add(R.id.body_container, bodyPartFragment).commit();
            fragmentManager.beginTransaction().add(R.id.legs_container, legsPartFragment).commit();
        }
    }
}
