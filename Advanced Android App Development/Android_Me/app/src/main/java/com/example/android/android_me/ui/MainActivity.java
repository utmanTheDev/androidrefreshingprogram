package com.example.android.android_me.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.android.android_me.R;
import com.example.android.android_me.data.AndroidImageAssets;

public class MainActivity extends AppCompatActivity implements MasterListFragment.OnImageClickListener {
    private Boolean twoPanels;
    private int headIndex;
    private int bodyIndex;
    private int legsIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (findViewById(R.id.android_me_linear_layout) != null) {
            twoPanels = true;
            if (savedInstanceState == null) {
                BodyPartFragment bodyPartFragment = new BodyPartFragment();
                BodyPartFragment headPartFragment = new BodyPartFragment();
                BodyPartFragment legsPartFragment = new BodyPartFragment();

                headPartFragment.setImageIds(AndroidImageAssets.getHeads());
                headPartFragment.setListIndex(headIndex);

                bodyPartFragment.setImageIds(AndroidImageAssets.getBodies());
                bodyPartFragment.setListIndex(bodyIndex);

                legsPartFragment.setImageIds(AndroidImageAssets.getLegs());
                legsPartFragment.setListIndex(legsIndex);

                FragmentManager fragmentManager = getSupportFragmentManager();

                fragmentManager.beginTransaction().add(R.id.head_container, headPartFragment).commit();
                fragmentManager.beginTransaction().add(R.id.body_container, bodyPartFragment).commit();
                fragmentManager.beginTransaction().add(R.id.legs_container, legsPartFragment).commit();
            } else {
                twoPanels = true;
            }
        }
    }

    @Override
    public void onImageSelected(int position) {
        Toast.makeText(this, "Item clicked : " + position, Toast.LENGTH_SHORT).show();
        int bodyPartNum = position / 12;
        int listIndex = position - 12 * bodyPartNum;
        switch (bodyPartNum) {
            case 0:
                headIndex = listIndex;
                break;
            case 1:
                bodyIndex = listIndex;
                break;
            case 2:
                legsIndex = listIndex;
                break;
            default:
                break;
        }

        Bundle bundle = new Bundle();
        bundle.putInt("headIndex", headIndex);
        bundle.putInt("bodyIndex", bodyIndex);
        bundle.putInt("legsIndex", legsIndex);
        final Intent intent = new Intent(this, AndroidMeActivity.class);
        intent.putExtras(bundle);

        Button buttonToDetail = findViewById(R.id.buttonToDetail);
        buttonToDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent);
            }
        });
    }
}
