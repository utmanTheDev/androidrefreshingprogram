package com.example.android.emojify;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.SparseArray;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import timber.log.Timber;

public class Emojifier {
    private static final float EYE_OPEN_PROBAVILITY = 0.7f;
    private static final float SMILING_PROBALITY = 0.5f;

    private static final float EMOJI_SCALE_FACTOR = .9f;

    private static final String TAG = "XXXX";

    static Bitmap detectFacesAndOverlay(Context context, Bitmap bitmap) {

        FaceDetector detector = new FaceDetector.Builder(context)
                .setTrackingEnabled(false)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                .build();

        Frame frame = new Frame.Builder().setBitmap(bitmap).build();

        SparseArray<Face> faces = detector.detect(frame);

        Bitmap resultBitmap = bitmap;

        if (faces.size() != 0) {
            Timber.d("detectedFaces: " + faces.size());
            for (int i = 0; i < faces.size(); i++) {
                Bitmap emojiBitmap;
                //getClasifications(faces.valueAt(i));
                EmojiStatus emojiStatus = whichEmoji(faces.valueAt(i));

                switch (emojiStatus) {
                    case BOTH_OPEN_SMILING:
                        emojiBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.smile);
                        break;
                    case LEFT_OPEN_SMILING:
                        emojiBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.leftwink);
                        break;
                    case RIGHT_OPEN_SMILING:
                        emojiBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.rightwink);
                        break;
                    case BOTH_CLOSE_SMILING:
                        emojiBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.closed_smile);
                        break;
                    case LEFT_CLOSE_SMILING:
                        emojiBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.smile);
                        break;
                    case RIGHT_CLOSE_SMILING:
                        emojiBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.smile);
                        break;
                    case BOTH_OPEN_SERIOUS:
                        emojiBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.frown);
                        break;
                    case LEFT_OPEN_SERIOUS:
                        emojiBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.leftwinkfrown);
                        break;
                    case RIGHT_OPEN_SERIOUS:
                        emojiBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.rightwinkfrown);
                        break;
                    case BOTH_CLOSE_SERIOUS:
                        emojiBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.closed_frown);
                        break;
                    case LEFT_CLOSE_SERIOUS:
                        emojiBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.frown);
                        break;
                    case RIGHT_CLOSE_SERIOUS:
                        emojiBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.frown);
                        break;
                    default:
                        emojiBitmap = null;
                        Toast.makeText(context, R.string.no_emoji, Toast.LENGTH_LONG).show();
                }

                resultBitmap = addBitmapToFace(bitmap, emojiBitmap, faces.valueAt(i));
            }
        } else {
            Toast.makeText(context, "No faces detected", Toast.LENGTH_SHORT).show();
        }

        detector.release();

        return resultBitmap;
    }

    static void getClasifications(Face face) {
        float rightOpen = face.getIsRightEyeOpenProbability();
        float leftOpen = face.getIsLeftEyeOpenProbability();
        float smiling = face.getIsSmilingProbability();
        Timber.d("getClasifications Right Eye Open : " + rightOpen + " , Left Eye Open : " + leftOpen + " , Smiling : " + smiling);
    }

    static EmojiStatus whichEmoji(Face face) {
        float rightOpen = face.getIsRightEyeOpenProbability();
        float leftOpen = face.getIsLeftEyeOpenProbability();
        float smiling = face.getIsSmilingProbability();

        boolean isRightOpen = rightOpen > EYE_OPEN_PROBAVILITY;
        boolean isLeftOpen = leftOpen > EYE_OPEN_PROBAVILITY;
        boolean isSmiling = smiling > SMILING_PROBALITY;

        EmojiStatus emojiStatus;
        if (isSmiling) {
            if (isLeftOpen && isRightOpen) {
                emojiStatus = EmojiStatus.BOTH_OPEN_SMILING;
            } else if (isLeftOpen && !isRightOpen) {
                emojiStatus = EmojiStatus.LEFT_OPEN_SMILING;
            } else if (!isLeftOpen && isRightOpen) {
                emojiStatus = EmojiStatus.RIGHT_OPEN_SMILING;
            } else {
                emojiStatus = EmojiStatus.BOTH_CLOSE_SMILING;
            }
        } else {
            if (isLeftOpen && isRightOpen) {
                emojiStatus = EmojiStatus.BOTH_OPEN_SERIOUS;
            } else if (isLeftOpen && !isRightOpen) {
                emojiStatus = EmojiStatus.LEFT_OPEN_SERIOUS;
            } else if (!isLeftOpen && isRightOpen) {
                emojiStatus = EmojiStatus.RIGHT_OPEN_SERIOUS;
            } else {
                emojiStatus = EmojiStatus.BOTH_CLOSE_SERIOUS;
            }
        }

        Timber.d("whichEmoji: " + emojiStatus.name());
        Timber.d("getClasifications Right Eye Open : " + rightOpen + " , Left Eye Open : " + leftOpen + " , Smiling : " + smiling);
        return emojiStatus;
    }

    private static Bitmap addBitmapToFace(Bitmap backgroundBitmap, Bitmap emojiBitmap, Face face) {

        // Initialize the results bitmap to be a mutable copy of the original image
        Bitmap resultBitmap = Bitmap.createBitmap(backgroundBitmap.getWidth(), backgroundBitmap.getHeight(), backgroundBitmap.getConfig());

        // Scale the emoji so it looks better on the face
        float scaleFactor = EMOJI_SCALE_FACTOR;

        // Determine the size of the emoji to match the width of the face and preserve aspect ratio
        int newEmojiWidth = (int) (face.getWidth() * scaleFactor);
        int newEmojiHeight = (int) (emojiBitmap.getHeight() * newEmojiWidth / emojiBitmap.getWidth() * scaleFactor);

        // Scale the emoji
        emojiBitmap = Bitmap.createScaledBitmap(emojiBitmap, newEmojiWidth, newEmojiHeight, false);

        // Determine the emoji position so it best lines up with the face
        float emojiPositionX = (face.getPosition().x + face.getWidth() / 2) - emojiBitmap.getWidth() / 2;
        float emojiPositionY = (face.getPosition().y + face.getHeight() / 2) - emojiBitmap.getHeight() / 3;

        // Create the canvas and draw the bitmaps to it
        Canvas canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(backgroundBitmap, 0, 0, null);
        canvas.drawBitmap(emojiBitmap, emojiPositionX, emojiPositionY, null);

        return resultBitmap;
    }

    enum EmojiStatus {
        BOTH_OPEN_SMILING,
        RIGHT_OPEN_SMILING,
        LEFT_OPEN_SMILING,

        BOTH_OPEN_SERIOUS,
        RIGHT_OPEN_SERIOUS,
        LEFT_OPEN_SERIOUS,

        BOTH_CLOSE_SMILING,
        RIGHT_CLOSE_SMILING,
        LEFT_CLOSE_SMILING,

        BOTH_CLOSE_SERIOUS,
        RIGHT_CLOSE_SERIOUS,
        LEFT_CLOSE_SERIOUS
    }
}
