package com.example.android.teatime;

import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.anything;

@RunWith(AndroidJUnit4.class)
public class MenuActivityScreenTest {
    @Rule
    public ActivityTestRule<MenuActivity> activityRule = new ActivityTestRule(MenuActivity.class);

    @Test
    public void clickAdapterViewItem() {
        onData(anything()).inAdapterView(withId(R.id.tea_grid_view)).atPosition(1).perform(click());

        // Vista del OrderActivity
        onView(withId(R.id.tea_name_text_view)).check(matches(withText("Green Tea")));
    }
}
