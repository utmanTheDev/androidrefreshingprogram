package com.example.android.shushme;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;

import java.util.ArrayList;
import java.util.List;

public class GeoFencing implements ResultCallback {
    private static final String TAG = GeoFencing.class.getName();
    private static final long GEOFENCE_TIMEOUT = 60 * 60 * 1000; // 1 hour
    private static final float GEOFENCE_RADIUS = 50; // 50 meters


    private PendingIntent geofencingPendingIntent;
    private GoogleApiClient googleApiClient;
    private List<Geofence> geofenceList;
    private Context context;

    public GeoFencing(Context context, GoogleApiClient googleApiClient) {
        this.googleApiClient = googleApiClient;
        this.geofenceList = new ArrayList<>();
        this.geofencingPendingIntent = null;
        this.context = context;
    }

    public void updateGeofencesList(PlaceBuffer places) {
        geofenceList = new ArrayList<>();
        if (places == null || places.getCount() == 0) return;

        for (Place place : places) {
            String placeId = place.getId();
            double placeLat = place.getLatLng().latitude;
            double placeLong = place.getLatLng().latitude;

            Geofence geofence = new Geofence.Builder()
                    .setRequestId(placeId)
                    .setExpirationDuration(GEOFENCE_TIMEOUT)
                    .setCircularRegion(placeLat, placeLong, GEOFENCE_RADIUS)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                    .build();
        }
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(geofenceList);
        return builder.build();
    }

    private PendingIntent getGeofencingPendingIntent() {
        if (geofencingPendingIntent != null) {
            return geofencingPendingIntent;
        } else {
            Intent intent = new Intent(context, GeofenceBroadcastReceiver.class);
            geofencingPendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            return geofencingPendingIntent;
        }
    }

    public void registerAllGeofences() {
        if (googleApiClient == null || !googleApiClient.isConnected() || geofenceList == null || geofenceList.size() == 0) {
            return;
        } else {
            try {
                LocationServices.GeofencingApi.addGeofences(
                        googleApiClient,
                        getGeofencingRequest(),
                        getGeofencingPendingIntent()
                ).setResultCallback(this);
            } catch (SecurityException securityException) {
                Log.e(TAG, "registerAllGeofences: ", securityException);
            }
        }
    }

    public void unregisterAllGeofences() {
        if (googleApiClient == null || !googleApiClient.isConnected()) {
            return;
        } else {
            try {
                LocationServices.GeofencingApi.removeGeofences(
                        googleApiClient,
                        getGeofencingPendingIntent()
                ).setResultCallback(this);
            } catch (SecurityException securityException) {
                Log.e(TAG, "registerAllGeofences: ", securityException);
            }
        }
    }

    @Override
    public void onResult(@NonNull Result result) {
        Log.e(TAG, "onResult: Error adding/removing geofence : " + result.getStatus());
    }
}
